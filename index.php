<?php include_once "core/calculator.php" ?>
<!doctype html>
<html lang="ru">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <title>Задача № 2</title>
</head>
<body>
<?php include_once "newyear/newyear.php" ?>
<div class="container">
    <div class="alert alert-success" role="alert">
        <h3> Результат вычислений : <span id="result"></span></h3>
    </div>

    <form class="row gy-2 gx-3 align-items-center" method="post" action="" id="post_form" name="post_form">
        <div class="col-auto">
            <input type="number" class="form-control" id="input1" name="input1" step="0.01" placeholder="число 1 ...">
        </div>

        <div class="col-auto">

            <select class="form-select" id="autoSizingSelect" name="operator">
                <option value="0" selected> действие</option>
                <option value="1"> +</option>
                <option value="2"> -</option>
                <option value="3"> *</option>
                <option value="4"> :</option>
            </select>
        </div>

        <div class="col-auto">
            <div class="input-group">
                <input type="number" class="form-control" id="input2" name="input2" step="0.01"
                       placeholder="число 2 ... ">
            </div>
        </div>

        <div class="col-auto">
            <button type="submit" class="btn btn-primary" id="calculate" name="calculate">Вычислить</button>
        </div>
        <div class="col-auto">
            <button type="submit" class="btn btn-danger" id="del" name="del">Очистить</button>
        </div>
    </form>


</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>

<!--<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js"-->
<!--        integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj"-->
<!--        crossorigin="anonymous"></script>-->
<script src="/js/post.js"></script>
</body>
</html>