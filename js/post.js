$(function () {

    $('#post_form').submit(function () {
        $.post(
            '/core/calculator.php', // адрес обработчика
            $("#post_form").serialize(), // отправляемые данные

            function (e) { // получен ответ сервера
                // $('#post_form').hide('slow');
                $('#result').html(e);
            }
        );
        return false;

    });

    $("#del").click(function () {
        $('#post_form')[0].reset();
    });

})